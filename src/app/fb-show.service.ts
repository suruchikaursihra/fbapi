import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FbShowService {

  constructor() { }

  servicelog;

  getData(){
    return this.servicelog;
  }

  setData(data){
    this.servicelog=data;
  }

}
