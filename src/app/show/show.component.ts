import { Component, OnInit } from '@angular/core';
import { FbShowService } from '../fb-show.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {
userData: any;
  constructor(private servicelog : FbShowService,private route: Router) { 
  

  }

  ngOnInit() {
this.userData=this.servicelog.getData();

  }

}
