import { TestBed, inject } from '@angular/core/testing';

import { FbShowService } from './fb-show.service';

describe('FbShowService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FbShowService]
    });
  });

  it('should be created', inject([FbShowService], (service: FbShowService) => {
    expect(service).toBeTruthy();
  }));
});
