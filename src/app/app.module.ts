import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes,Router } from '@angular/router';

import { AppComponent } from './app.component';
import { FacebookLoginProvider, AuthServiceConfig, SocialLoginModule} from "angular-6-social-login";
import { SigninComponent } from './signin/signin.component';
import { ShowComponent } from './show/show.component';
//import { FbShowService } from './fb-show.service';


export function getAuthServiceConfigs(){
  let config= new AuthServiceConfig([
    {
      id:FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider('240640423228448')
    }
  ]);
  return config;
}

const route: Routes = [
  {
    path:'',
    component: SigninComponent
  },
  {
    path:'show',
    component: ShowComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    ShowComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(route),
    SocialLoginModule
  ],
  providers: [
    {
      provide:AuthServiceConfig,
      useFactory:getAuthServiceConfigs
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
